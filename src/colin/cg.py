from numpy import *
from numpy.linalg import norm
import matplotlib.pyplot as plt
from pylab import *

def solve_cg(A,b,x0,rtol,maxits):

	"""
    Function to use the conjugate gradient algorithm to
    solve the equation Ax = b for symmetric positive definite A.
    Inputs:
    A -- An nxn matrix stored as a rank-2 numpy array
    b -- A length n vector stored as a rank-1 numpy array
    x0 -- The initial guess, length n vector stored as a rank-1 numpy array
    rtol -- a tolerance, algorithm should stop if l2-norm of 
    the residual r=Ax-b drops below this value.
    maxits -- Stop if the tolerance has not been reached after this number
    of iterations
    
    Outputs:
    x -- the approximate solution
    rvals -- a numpy array containing the l2 norms of the residuals
    r=Ax-b at each iteration
    """
	
	n = len(x0)


	xOld = x0
	x = x0

	r = b

	rvals = []

	rOld = b - dot(A,x0)

	ts = range(1,maxits)
		
	for k in ts:

		if norm(b - dot(A,xOld)) < rtol:
			ts = range(1,k)
			break
	
		if k == 1:
			p = rOld
		else: 
			p = rOld + (norm(rOld)**2)/(norm(rOlder)**2)*pOld


		alpha = (norm(rOld)**2)/(inner(p,dot(A,p)))
		
		# Update x, r and p values
		x = xOld + alpha*p
		xOld = x.copy()		
		r = rOld - alpha*(dot(A,p))
		rOlder = rOld.copy()
		rOld = r.copy()	
		pOld = p.copy()	

		rvals.append(norm(b - dot(A,x)))
		
	plt.plot(ts, rvals)	
	plt.yscale('log')
	plt.show() 

	
	print len(rvals)
	print len(ts)
		
	return x, rvals

