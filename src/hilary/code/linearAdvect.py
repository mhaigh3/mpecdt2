#!/usr/bin/python

# Outer code for setting up the linear advection problem on a uniform
# grid and calling the function to perform the linear advection and plot.
# The function takes an argument which gives the name of the input file

### Copy out most of this code. Code commented with 3#s (like this) ###
### is here to help you to learn python and need not be copied      ###

### The command at the top means that this python function can be  ###
### run directly from the command line (you will also need to do   ###
### "chmod -u linearAdvect" in unix or linux                       ###

### Note that blocks are defined by indendtation in Python. You     ###
### should never mix tabs and spaces for indentation - use 4 spaces.###
### Setup your text editor to insert 4 spaces when you press tab    ###

### If you are using Python 2.7 rather than Python 3, import various###
### functions from Python 3 such as to use real number division     ###
### rather than integer division. ie 3/2  = 1.5  rather than 3/2 = 1###
from __future__ import absolute_import, division, print_function

### The sys module provides access to operating system commands and ###
### access to command line variables                                ###
import sys

# read in all the linear advection schemes, initial conditions and other
# code associated with this application
from grid import *
from initialConditions import *
from advectionSchemes import *
from diagnostics import *

### The main code is inside a function to avoid global variables    ###
### The arguments (argv) give access to the command line arguments  ###
def main(argv):
    ### Test the command line argument and from these set the       ###
    ### name of the input file.                                     ###
    if len(sys.argv) != 2:
        print('usage: linearAdvect <inputFile.py>')
        sys.exit(2)
    inputFile = sys.argv[1]
    print('Reading input from file ', inputFile)

    # Define input variables from the input file and store in dictionary 
    # inputs
    inputs={}
    execfile(inputFile, inputs)
    
    print('Advecting the profile ...')

    ### Declare an instance of the Grid class, called grid which    ###
    ### defines the grid for these simulations. Thus grid.dx and    ###
    ### grid.x will automatically be defined
    grid = Grid(inputs['nx'], inputs['xmin'], inputs['xmax'])

    # Other input variables
    c = inputs['c']                # Courant number for the advection
    nt = inputs['nt']              # Total number of time steps to run for
    # Select which initial conditions to use (from initialConditions.py)
    initialConditions = eval(inputs['initialConditions'])
    
    # Initialise dependent variable
    phiOld = initialConditions(grid.x)
    # Exact solution is the initial condition shifted around the domain
    phiExact = initialConditions((grid.x - c*nt*grid.dx)%grid.length)

    # Advect the profile using finite difference for all the time steps
    phiFTCS = FTCS(phiOld.copy(), c, nt)
    
    # calculate the error norms, mean and standard deviation
    noErrors = errorDiagnostics(grid, phiExact, phiExact)
    noErrors.write("Exact")
    FTCSerrors = errorDiagnostics(grid, phiFTCS, phiExact)
    FTCSerrors.write("FTCS")

    ### Create plot with two curves and two labels and save figure  ###
    ### to the outFile                                              ###
    plotFinal(grid.x, [phiExact, phiFTCS],
              ['Exact Solution', 'FTCS'], inputs['outFile'])

### Run the function main defined in this file with one argument    ###
if __name__ == "__main__":
   main(sys.argv[1:])

