`Back to Numerics Course Home Page <http://mpecdt.bitbucket.org/>`_

Videos for Part I of the Numerics Course (Hilary Weller)
========================================================

Week 1 (5-9 Oct 2015) Introduction and and Linear Advection
-----------------------------------------------------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=gnqFGE5G9rY','MyWindow','width=900,height=600'); return false;"> Video 1: Introduction</a><br>

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=J9dJ8hkacS4','MyWindow','width=900,height=600'); return false;"> Video 2: Atmospheric Dynamics</a><br>

    <a href="#" onClick="MyWindow=window.open('https://www.youtube.com/watch?v=qLOEghXjXTg','MyWindow','width=900,height=600'); return false;"> Video 3: Linear Advection</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/nfi12uod0y8','MyWindow','width=900,height=600'); return false;"> Video 4: Fourier Analysis</a><br>

Week 2 (12-16 Oct 2015) Numerical Analysis
------------------------------------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/0mWdMjoVVoo','MyWindow','width=900,height=600'); return false;"> Video 5.0: Introduction to Numerical Analysis</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/NNV4onwTDnc','MyWindow','width=900,height=600'); return false;"> Video 5.1: Definitions of Terms</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/ecE9X6zop9U','MyWindow','width=900,height=600'); return false;"> Video 5.2: The domain of dependence </a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/C9kGgbV2BWE','MyWindow','width=900,height=600'); return false;"> Video 5.3: Von-Neumann Stability Analysis </a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/CBgffjDbco4','MyWindow','width=900,height=600'); return false;"> Video 5.4: Conservation </a><br>

Week 3 Dispersion
-----------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/Zt2mvygV5lM','MyWindow','width=900,height=600'); return false;"> Video 6.0: Introduction to Dispersion</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/zd_bzGpPW4s','MyWindow','width=900,height=600'); return false;"> Video 6.0: Numerical Dispersion Errors</a><br>

Week 4 Alternative Advection Schemes
------------------------------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/g7hwZ4iVv78','MyWindow','width=900,height=600'); return false;"> Video 7.0: Introduction (2:15)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/egnsVOvJYIA','MyWindow','width=900,height=600'); return false;"> Video 7.1: Semi-Lagrangian (6:38)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/K343wYgdsOc','MyWindow','width=900,height=600'); return false;"> Video 7.2: Artificial Diffusion to Control Spurious Oscillations (2:58)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/fGXxS016YAE','MyWindow','width=900,height=600'); return false;"> Video 7.3: The Finite Volume Method (7:14) </a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/PT0bvFU6rhw','MyWindow','width=900,height=600'); return false;"> Video 7.4: The Lax-Wendroff Advection Scheme (4:44) </a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/sz_6hw0vEa4','MyWindow','width=900,height=600'); return false;"> Video 7.5: Total Variation Diminishing Schemes (10:36) </a><br>

Week 5 Modelling the Shallow Water Equations
------------------------------------

.. raw:: html

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/gQ7KGhuE1e8','MyWindow','width=900,height=600'); return false;"> Video 8.0: Introduction (5:44)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/S_cvGEQ3hwo','MyWindow','width=900,height=600'); return false;"> Video 8.1:Linearisation of the Shallow Water Equations (3:27)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/1wWHqltukXo','MyWindow','width=900,height=600'); return false;"> Video 8.2: A first numerical method and its analysis (6:33)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/rMr6HIzxmbw','MyWindow','width=900,height=600'); return false;"> Video 8.3: Grid-scale oscillations and numerical dispersion (4:55)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/JFblO6l22TM','MyWindow','width=900,height=600'); return false;"> Video 8.4: A C-grid staggered numerical method (4:02)</a><br>

    <a href="#" onClick="MyWindow=window.open('https://youtu.be/iF_D2gnDJDU','MyWindow','width=900,height=600'); return false;"> Video 8.5: Arakawa grids for 2D modelling (4:49)</a><br>

