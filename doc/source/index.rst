.. Finite element course documentation master file, created by
   sphinx-quickstart on Sat Sep  6 21:48:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Resources for Computational Science
===================================

`Kick off camp Python course <http://mpecdt.github.io/Introduction-to-Python-programming-for-MPECDT/>`_

.. toctree::
   :maxdepth: 1

   tools
   irc

Numerics course Part I (Hilary Weller)
======================================

* `Lecture notes for weeks 1-5 <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/PDEsNumerics_2_student.pdf>`_
    Print out these lecture notes so that you can fill in the gaps during the videos

* `Lecturer version of the notes for weeks 1-5 <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/PDEsNumerics_2_lec_growChew856.pdf>`_
    You can refer to these to check your answers (gaps filled in)

* `University of Reading Blackboard <https://www.bb.reading.ac.uk>`_
    For discussion cite, announcements and handing in reports (pdf format)

* `Corrections to the notes <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/changes.pdf>`_
    If you spot errors in the notes, please `email me <h.weller@reading.ac.uk>`_ and I will give you a chocolate for your troubles. The notes will be corrected and a list of dated corrections is available `here <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/changes.pdf>`_

.. toctree::
   :maxdepth: 1

   hilary/announcements
   hilary/assignments
   hilary/videos

* `Lecture notes and videos on using Taylor series to find finite difference approximations <https://www.youtube.com/playlist?list=PLEG35I51CH7UttDft9tOB0ej_kkAJF368>`_

* `Cubic Lagrange interpolation <http://www.met.reading.ac.uk/~sws02hs/teaching/PDEsNumerics/cubicLagrange.pdf>`_


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

